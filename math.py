__author__ = 'Worker'
import socket, struct
from cidrize import cidrize

def int2ip(addr):
    return socket.inet_ntoa(struct.pack("!I", addr))

start = int2ip(39781888)
stop = int2ip(39782143)

IP = start + '-' + stop
print cidrize(IP)


"'[^<]+?'"